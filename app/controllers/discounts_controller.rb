class DiscountsController < ApplicationController
  before_action :set_discount, only: [:show, :edit, :update, :destroy]
  before_action :get_helper_datas, only: [:new, :show, :edit, :update, :destroy]

  def index
    @discounts = Discount.all
  end


  def new
    @discount = Discount.new
    @products = Product.all.map { |product| [product.name, product.id] }
    @customers = Customer.all.map { |customer| [customer.name, customer.id] }
  end


  def create
    @discount = Discount.new(discount_params)
    respond_to do |format|
      if @discount.save
        format.html { redirect_to discounts_path, notice: 'discount was successfully created.' }
        format.json { render :nothing => true, :status => 204 }
      else
        @products = Product.all.map { |product| [product.name, product.id] }
        @customers = Customer.all.map { |customer| [customer.name, customer.id] }
        format.html { render :new }
        format.json { render json: @discount.errors, status: :unprocessable_entity }
      end
    end
  end


  def edit
    @discount_type = DiscountsHelper::DISCOUNT_TYPE.map { |discount_type| discount_type }
    @status = DiscountsHelper::DISCOUNT_STATUS.map { |discount_status| discount_status }
    product = @discount.product
    @products = [[product.name, product.id]]
    customer = @discount.customer
    @customers = [[customer.name, customer.id]]
  end


  def update
    respond_to do |format|
      if @discount.update(discount_params)
        format.html { redirect_to discounts_path, notice: 'discount was successfully updated.' }
        format.json { render :nothing => true, :status => 204 }
      else
        format.html { render :edit }
        format.json { render json: @discount.errors, status: :unprocessable_entity }
      end
    end
  end


  def destroy
    @discount.destroy
    respond_to do |format|
      format.html { redirect_to discounts_url, notice: 'discount was successfully deleted.' }
      format.json { head :no_content }
    end
  end

  private
    def set_discount
      @discount = Discount.find_by(id: params[:id])
    end

    def get_helper_datas
      @discount_type = DiscountsHelper::DISCOUNT_TYPE.map { |discount_type| discount_type }
      @status = DiscountsHelper::DISCOUNT_STATUS.map { |discount_status| discount_status }
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def discount_params
      params.require(:discount).permit(:product_id, :customer_id, :discount_type, :rate, :status, :minimum_purchase)
    end
end
