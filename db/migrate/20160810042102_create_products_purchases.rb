class CreateProductsPurchases < ActiveRecord::Migration[5.0]
  def change
    create_table :products_purchases do |t|
      t.integer :product_id, null: false
      t.integer :purchase_id, null: false

      t.timestamps
    end
  end
end
