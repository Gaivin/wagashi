class CreateDiscounts < ActiveRecord::Migration[5.0]
  def change
    create_table :discounts do |t|
      t.integer :product_id, null: false
      t.integer :customer_id, null: false
      t.integer :type, default: 0
      t.decimal :rate, :precision => 8, :scale => 5
      t.integer :status
      t.timestamps
    end
  end
end
